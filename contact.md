---
title: Contato
date: '2017-11-01T03:00:00.000+00:00'
banner_image: "/uploads/2018/12/07/compass.jpg"
heading: Contato através do e-mail
publish_date: '2017-12-01T04:00:00.000+00:00'
show_staff: false
hero_button:
  text: GitHub
  href: https://github.com/miguel7penteado
sub_heading: Parte de alguns trabalhos no GitHub
menu:
  navigation:
    identifier: _contact
    weight: 5

---
### Correio Eletrônico

[miguel7penteado at gmail.com](mailto:miguel7penteado@gmail.com)