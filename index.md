---
title: Miguel Penteado
banner_image: "/uploads/2019/08/15/lamps.jpg"
layout: landing-page
heading: Página pessoal
partners: []
services: []
sub_heading: Miguel Penteado
textline: Entusiasta de Ciência de Dados e fontes oficiais de dados públicos
hero_button:
  text: Iniciar
  href: "/about"
show_news: true
show_staff: false
menu:
  navigation:
    identifier: _index
    weight: 1
    title: Home

---
